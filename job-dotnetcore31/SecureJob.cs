﻿using AiCommon.Main.Job;
using AiCommon.Main.Roles;
using CommandLine;
using Microsoft.Extensions.Logging;

namespace job_dotnetcore31
{
    [Verb("secure-job", HelpText = "simple secure job")]
    public class SecureJobConfig : JobDeclaration<SecureJob, SecureJobConfig>
    {
    }

    [NeedRoles("capability-1")]
	public class SecureJob : IJob<SecureJobConfig>
    {
	    private readonly ILogger<SecureJob> _logger;

		public SecureJob(ILogger<SecureJob> logger)
		{
			_logger = logger;
		}

		public int Run(SecureJobConfig config)
        {
			_logger.LogWarning($"Hello World (from secure-job)");
			return 0;
        }

    }
}