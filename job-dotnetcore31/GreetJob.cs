﻿using AiCommon.Main.Job;
using CommandLine;
using Microsoft.Extensions.Logging;

namespace job_dotnetcore31
{
    [Verb("insecure-job", HelpText = "simple job")]
    public class GreetJobConfig : JobDeclaration<GreetJob, GreetJobConfig>
    {
	    public GreetJobConfig()
	    {
	    }
    }

    public class GreetJob : IJob<GreetJobConfig>
	{
		private readonly ILogger<GreetJob> _logger;

		public GreetJob(ILogger<GreetJob> logger)
		{
			_logger = logger;
		}

		public int Run(GreetJobConfig options)
		{
			_logger.LogWarning("Hello World (from insecure-job)");
			
			return 0;
		}
	}

}
