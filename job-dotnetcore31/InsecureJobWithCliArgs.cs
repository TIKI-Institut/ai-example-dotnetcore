﻿using AiCommon.Main.Job;
using CommandLine;
using Microsoft.Extensions.Logging;

namespace job_dotnetcore31
{
    [Verb("insecure-job-with-cli-args", HelpText = "simple job with arguments")]
    public class InsecureJobWithCliArgsConfig : JobDeclaration<InsecureJobWithCliArgs, InsecureJobWithCliArgsConfig>
    {

        [Option("count", HelpText = "Number of greetings")]
        public int Count { get; set; }

        [Option("name", HelpText = "The person to greet.")]
        public string Name { get; set; }
    }


    public class InsecureJobWithCliArgs : IJob<InsecureJobWithCliArgsConfig>
    {
	    private readonly ILogger<InsecureJobWithCliArgs> _logger;

	    public InsecureJobWithCliArgs(ILogger<InsecureJobWithCliArgs> logger)
	    {
		    _logger = logger;
	    }

	    public int Run(InsecureJobWithCliArgsConfig config)
		{
			for (var i = 0; i < config.Count; i++)
			{
				_logger.LogWarning($"Hello {config.Name}! {i}");
			}
			
			return 0;
		}
	}
}
