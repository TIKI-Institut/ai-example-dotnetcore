using System;

namespace web_dotnetcore31.Configuration
{
    public class EnvironmentConfigurationControllerConfig
    {
        public string StringValue { get; set; } 
        public int IntValue { get; set; } 
    }
}