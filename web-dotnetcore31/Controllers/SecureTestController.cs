﻿using AiCommon.Common.Auth.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace web_dotnetcore31.Controllers
{
	public class SecureTestController : ControllerBase
	{
		private DspOauthContext AuthContext { get; }

		public SecureTestController(DspOauthContext authContext)
		{
            AuthContext = authContext;
		}

		[Authorize(Roles = "capability-1")]
		[HttpGet]
		public string Get()
		{
			var name = AuthContext.Principal.GetClaimAttribute("name");

			return "Secure hello world! Hello: " + name;
		}
	}
}
