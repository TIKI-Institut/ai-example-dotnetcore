﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using web_dotnetcore31.Configuration;

namespace web_dotnetcore31.Controllers
{
    public class EnvironmentalConfigurationController : ControllerBase
    {
        private EnvironmentConfigurationControllerConfig _controllerConfig;

        public EnvironmentalConfigurationController(IOptions<EnvironmentConfigurationControllerConfig> controllerConfig)
        {
            _controllerConfig = controllerConfig.Value;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult(new Dictionary<string, object>
                {{"stringValue", _controllerConfig.StringValue}, {"intValue", _controllerConfig.IntValue}});
        }
    }
}