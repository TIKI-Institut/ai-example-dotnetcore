﻿using Microsoft.AspNetCore.Mvc;

namespace web_dotnetcore31.Controllers
{
	public class TestController : ControllerBase
	{
		[HttpGet]
		public string Get()
		{
			return "Hello world!";
		}
	}
}
