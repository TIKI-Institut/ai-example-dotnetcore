﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace web_dotnetcore31.Controllers
{
    public class ResourcesController: ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            var cpuLimit = Environment.GetEnvironmentVariable("CPU_LIMIT") ?? "unavailable";
            var memoryLimit = Environment.GetEnvironmentVariable("MEMORY_LIMIT") ?? "unavailable";
            return $"cpu limit: {cpuLimit} m, memory limit {memoryLimit} Mi";
        }
    }
}