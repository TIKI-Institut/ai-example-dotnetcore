using System.IO;
using AiCommon.Common.ApplicationStartup;
using AiCommon.Common.ApplicationStartup.Initializers;
using AiCommon.Main.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Samhammer.DependencyInjection.Attributes;
using web_dotnetcore31.Configuration;

namespace web_dotnetcore31.Application
{
    [InjectAs(typeof(IModularApplicationInitialization))]
    public class AppInitialization : IModularApplicationInitialization
    {
        private readonly IConfiguration _baseConfiguration;
        private readonly IHostingEnvironment _environment;

        public AppInitialization(IConfiguration baseConfiguration, IHostingEnvironment environment)
        {
            _baseConfiguration = baseConfiguration;
            _environment = environment;
        }

        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            
            //you can use _baseConfiguration to read values from the previously configured configuration
            
            //but you can setup your own configuration as well...
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_environment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables(source => source.Prefix = "AI_EXAMPLE_DOTNET_CORE_");

            var configuration = builder.Build();

            var managerConfig = configuration.GetSection("AiExampleDotnetCore");

            serviceCollection
                .UseApplicationServiceInitializer<SerilogInitializer>()
                .AddOptions()
                .Configure<EnvironmentConfigurationControllerConfig>(
                    managerConfig.GetSection("EnvironmentConfigurationController"));

        }

        public string ApplicationSelector => WebMain.MainAppSelector;
    }
}