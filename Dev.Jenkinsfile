@Library(value='tiki/jenkins', changelog=false) _

pipeline {
    agent {
        kubernetes {
            defaultContainer 'dotnetcore31'
            yamlMergeStrategy merge()
            inheritFrom 'dsp-dotnetcore-31 dsp-provision dsp-integration-test'
            yaml """
                apiVersion: v1
                kind: Pod
                metadata:
                spec:
                  containers:
                  - name: provisioner
                    image: docker.tiki-dsp.io/cicd-provisioner:${params.PROVISIONER_VERSION}
                """
        }
    }

    options {
        buildDiscarder(logRotator(daysToKeepStr: '14'))
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    parameters {
        string(name: 'KERNEL_VERSION', defaultValue: 'latest', description: 'ai-kernel version')
        string(name: 'PROVISIONER_VERSION', defaultValue: 'latest', description: 'cicd-provisioner version')
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    environment {
        PRINCIPAL   = 'tiki-test'
        PROJECT     = 'ai-example-dotnetcore'
        ENVIRONMENT = aiExampleEnvironment(env.BRANCH_NAME, params.KERNEL_VERSION, params.PROVISIONER_VERSION)
        PROVISIONER_DSPDOMAIN = 'tiki-dsp.tech'
    }

    stages {
        stage('Prepare') {
            steps {
                //bitbucketStatusNotify(buildState: 'INPROGRESS')
                echo "Starting pipeline for branch '${env.BRANCH_NAME}' with KERNEL_VERSION '${params.KERNEL_VERSION}' and PROVISIONER_VERSION '${params.PROVISIONER_VERSION}'"
            }
        }
        stage('Provisioner') {
            stages {
                stage('prepare') {
                    steps {
                        container('provisioner') {
                            script {
                                sh "mkdir -p /home/jenkins/backup"
                                generateCiMetaFile("${params.KERNEL_VERSION}", "${ENVIRONMENT}")
                            }
                        }
                    }
                }
                stage('decommission') {
                    steps {
                        container('provisioner') {
                            script {
                                try {
                                    sh "ai-provisioner -p /opt/.provisioner decommission ${WORKSPACE}"
                                } catch (ex) {
                                    echo 'Decommission failed, skipping...'
                                }
                            }
                        }
                    }
                }
                stage('setup project') {
                    steps {
                        container('provisioner') {
                            sh "ai-provisioner -p /opt/.provisioner setup ${WORKSPACE}"
                        }
                    }
                }
                stage('assign keycloak client roles') {
                    options { skipDefaultCheckout() }
                    steps {
                        container('keycloak') {
                            sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.tech/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                            sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT} --rolename dsp-deploy --rolename dsp-undeploy --rolename dsp-list --rolename dsp-decommission --rolename dsp-provision"
                        }
                    }
                }
                stage('provision') {
                    steps {
                        container('provisioner') {
                            sh "ai-provisioner -p /opt/.provisioner provision ${WORKSPACE}"
                        }
                    }
                }
                stage('assign keycloak roles for jobs') {
                    options { skipDefaultCheckout() }
                    steps {
                        container('keycloak') {
                            sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.tech/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                            sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT}-job-dotnetcore31-secure --rolename capability-1"

                        }
                    }
                }
                stage('deploy') {
                    steps {
                        container('provisioner') {
                            sh "ai-provisioner -p /opt/.provisioner deploy ${WORKSPACE}"
                        }
                    }
                }
                stage('check job status') {
                    steps {
                        container('kubectl') {
                            script {
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-dotnetcore31")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-dotnetcore31-with-args")
                                checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-dotnetcore31-secure")
                            }
                        }
                    }
                }
                stage('check web services') {
                    options { skipDefaultCheckout() }
                    steps {
                        container('keycloak') {
                            script {
                                // get roles to use secure endpoints
                                sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.tech/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                                sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT}-web-dotnetcore31 --rolename capability-1"
                                // check open endpoints
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-dotnetcore31/test", 15)
                                // get token for secure endpoint and save it in access_token.json
                                ACCESS_TOKEN = sh (
                                    script: "set +x \n curl --location --request POST 'https://auth.tiki-dsp.tech/auth/realms/${PRINCIPAL}/protocol/openid-connect/token' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'client_id=${PROJECT}-${ENVIRONMENT}' --data-urlencode 'grant_type=password' --data-urlencode username=\$(cat /opt/kc/credentials.json | jq -r .username) --data-urlencode password=\$(cat /opt/kc/credentials.json | jq -r .password) | jq -r .access_token |tr -d '\\n'",
                                    returnStdout: true
                                ).trim()
                                // check secure endpoints
                                checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.tech/${PROJECT}/${ENVIRONMENT}/web-dotnetcore31/securetest", bearerToken: "${ACCESS_TOKEN}", 15)
                            }
                        }
                    }
                }
            }
        }
    } // stages

    post {
        failure {
            doFailureSteps(slackNotification: true, bitbucketNotification: false)
        }
        fixed {
            doFixedSteps(slackNotification: true, bitbucketNotification: false)
        }
    }
}
